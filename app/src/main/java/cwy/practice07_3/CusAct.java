package cwy.practice07_3;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class CusAct extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cus);

        Button btn = (Button) findViewById(R.id.button3);
        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        EditText txt = null;
        txt = (EditText)findViewById(R.id.editText4);
        String custom = txt.getText().toString();

        SQLiteDatabase db = openOrCreateDatabase (
                "test.db",
                SQLiteDatabase.CREATE_IF_NECESSARY ,
                null
        );
        db.execSQL(custom);
        finish();
    }
}
