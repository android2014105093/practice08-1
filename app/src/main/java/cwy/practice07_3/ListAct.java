package cwy.practice07_3;

import android.app.ListActivity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListAdapter;

public class ListAct extends ListActivity {

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        loadDB();
    }
    @Override
    public void onResume() {
        super.onResume();
        loadDB();
    }

    public void loadDB() {
        ContentResolver cr = getContentResolver();

        Cursor cur = cr.query(Uri.parse("content://ex8/people"), null, null, null, null);

        ListAdapter adapt = new SimpleCursorAdapter(
                this,
                android.R.layout.simple_list_item_2,
                cur,
                new String[] {"name","age"},
                new int[] {android.R.id.text1,android.R.id.text2}, 0
        );
        setListAdapter(adapt);
    }
}
