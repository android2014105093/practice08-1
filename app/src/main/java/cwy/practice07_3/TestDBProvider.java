package cwy.practice07_3;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.text.TextUtils;

public class TestDBProvider extends ContentProvider {
    static final Uri CONTENT_URI =
            Uri.parse("content://ex8.test/people");
    static final int GETALL = 1;
    static final int GETONE = 2;

    static final UriMatcher matcher;
    static {
        matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI("ex8", "people", GETALL);
        matcher.addURI("ex8", "people/*", GETONE);
    }

    SQLiteDatabase mDB;
    class TestDBHelper extends SQLiteOpenHelper {
        public TestDBHelper(Context c) {
            super(c, "test.db", null, 1);
        }
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE IF NOT EXISTS people "
                    + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, age INTEGER);" );
        }
        public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer) {
            db.execSQL("DROP TABLE IF EXISTS people;");
            onCreate(db);
        }
    }

    public TestDBProvider() {
    }

    @Override
    public int delete(Uri arg0, String arg1, String[] arg2) {
        int cnt = 0;
        switch (matcher.match(arg0)) {
            case GETALL:
                cnt = mDB.delete("people", arg1, arg2);
                break;
            case GETONE:
                String where = "name = '" + arg0.getPathSegments().get(1) + "'";
                if (TextUtils.isEmpty(arg1) == false) {
                    where += " AND " + arg1;
                }
                cnt = mDB.delete("people", where, arg2);
                break;
        }
        getContext().getContentResolver().notifyChange(arg0, null);
        return cnt;
    }

    @Override
    public String getType(Uri arg0) {
        switch (matcher.match(arg0)) {
            case GETALL:
                return "vnd.android.cursor.dir/vnd.vr.people";
            case GETONE:
                return "vnd.android.cursor.item/vnd.vr.people";
        }
        return null;
    }

    @Override
    public Uri insert(Uri arg0, ContentValues arg1) {
        long row = mDB.insert("people", null, arg1);
        if (row > 0) {
            Uri notiuri = ContentUris.withAppendedId(CONTENT_URI, row);
            getContext().getContentResolver().notifyChange(notiuri, null);
            return notiuri;
        }
        return null;
    }

    @Override
    public boolean onCreate() {
        TestDBHelper helper = new TestDBHelper(getContext());
        mDB = helper.getWritableDatabase();
        return (mDB != null);
    }

    @Override
    public Cursor query(Uri arg0, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String sql;
        sql = "SELECT * FROM people";
        if (matcher.match(arg0) == GETONE) {
            sql += " WHERE name='";
            sql += arg0.getPathSegments().get(1);
            sql += "'";
        }
        sql += ";";
        Cursor cur = mDB.rawQuery(sql, null);
        return cur;
    }

    @Override
    public int update(Uri arg0, ContentValues arg1, String arg2,
                      String[] arg3) {
        int cnt = 0;
        switch (matcher.match(arg0)) {
            case GETALL:
                cnt = mDB.update("people", arg1, arg2, arg3);
                break;
            case GETONE:
                String where = "name = '" + arg0.getPathSegments().get(1) + "'";
                if (TextUtils.isEmpty(arg2) == false) {
                    where += " AND " + arg2;
                }
                cnt = mDB.update("people", arg1, where, arg3);
                break;
        }
        getContext().getContentResolver().notifyChange(arg0, null);
        return cnt;
    }
}
