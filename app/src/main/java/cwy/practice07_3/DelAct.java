package cwy.practice07_3;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class DelAct extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_del);

        Button btn = (Button) findViewById(R.id.button2);
        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        EditText txt = null;
        txt = (EditText)findViewById(R.id.editText3);
        String name = txt.getText().toString();

        ContentResolver cr = getContentResolver();

        cr.delete(Uri.parse("content://ex8/people/" + name), null, null);
        finish();
    }
}
