package cwy.practice07_3;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class InsAct extends AppCompatActivity implements View.OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ins);

        Button btn = (Button) findViewById(R.id.button1);
        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        EditText txt = null;
        txt = (EditText)findViewById(R.id.editText1);
        String name = txt.getText().toString();
        txt = (EditText)findViewById(R.id.editText2);
        String age = txt.getText().toString();

        ContentResolver cr = getContentResolver();
        ContentValues cv = new ContentValues();
        cv.put("name", name);
        cv.put("age", age);

        cr.insert(Uri.parse("content://ex8/people"), cv);
        finish();
    }
}
